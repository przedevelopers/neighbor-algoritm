package algortim.model;


import algortim.enums.ColorEnum;
import java.util.List;
import java.util.Random;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PopulationElement extends Rectangle {

    private final static int size = 1;
    private static final Random rnd = new Random();
    private static List<ColorEnum> colors;

    public PopulationElement(int width, int height) {
        super(width, height);

        addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                setFill(getPreviousColor());
            }

            if (event.getButton() == MouseButton.SECONDARY) {
                setFill(getNextColor());
            }
        });
    }

    public static PopulationElement of(int x, int y) {

        return of(ColorEnum.getListOfDefaultColors(), size, size, x, y);
    }

    public static PopulationElement of(List<ColorEnum> colorEnums, int width, int height, int x, int y) {
        colors = colorEnums;
        ColorEnum color = colors.get(rnd.nextInt(colors.size()));
        PopulationElement newElement = new PopulationElement(width, height);

        newElement.setFill(color.getColor());

        newElement.setLayoutX(x);
        newElement.setLayoutY(y);

        return newElement;
    }

    private Paint getNextColor() {
        int index = colors.indexOf(ColorEnum.of(getFill())) + 1;

        return index < colors.size()
                ? colors.get(index).getColor()
                : colors.get(0).getColor();
    }

    private Paint getPreviousColor() {
        int index = colors.indexOf(ColorEnum.of(getFill())) - 1;

        return index >= 0
                ? colors.get(index).getColor()
                : colors.get(colors.size() - 1).getColor();
    }

    @Override
    public String toString() {
        return Color.GOLD.toString();
    }
}
