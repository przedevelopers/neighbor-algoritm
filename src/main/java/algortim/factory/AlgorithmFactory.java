package algortim.factory;

import algortim.algorithms.Algorithm;
import algortim.algorithms.implementations.TwoColorsMaxOccursHorizontally;
import algortim.algorithms.implementations.TwoColorsMaxOccursVertically;
import algortim.enums.AlgorithmEnum;
import algortim.algorithms.implementations.FourColorsMaxOccurs;
import algortim.algorithms.implementations.RandomColorAlgorithm;
import java.util.HashMap;
import java.util.Map;

public abstract class AlgorithmFactory {
    private static Map<AlgorithmEnum, Algorithm> algorithms = new HashMap<AlgorithmEnum, Algorithm>() {{
        put(AlgorithmEnum.FOUR_COLOR_MAX_OCCURS, new FourColorsMaxOccurs());
        put(AlgorithmEnum.RANDOM_COLOR, new RandomColorAlgorithm());
        put(AlgorithmEnum.TWO_COLOR_MAX_OCCURS_HORIZONTALLY, new TwoColorsMaxOccursHorizontally());
        put(AlgorithmEnum.TWO_COLOR_MAX_OCCURS_VERTICALLY, new TwoColorsMaxOccursVertically());
    }};

    public static Algorithm getAlgorithm(AlgorithmEnum algorithmEnum, int width, int height) {
        if (!algorithms.containsKey(algorithmEnum)) {
            throw new IllegalArgumentException("Not supported algorithm enum: " + algorithmEnum);
        }

        algorithms.get(algorithmEnum).setHeight(height);
        algorithms.get(algorithmEnum).setWidth(width);

        return algorithms.get(algorithmEnum);
    }
}
