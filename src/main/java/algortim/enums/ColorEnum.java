package algortim.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ColorEnum {
    DEFAULT_COLOR("Default color", Color.BLACK),
    RED("Red", Color.RED),
    GREEN("Green", Color.GREEN),
    BLUE("Blue", Color.BLUE),
    YELLOW("Yellow", Color.YELLOW),
    GOLD("Gold", Color.GOLD),
    PINK("Pink", Color.PINK),
    ORANGE("Orange", Color.ORANGE),
        GRAY("Gray", Color.LIGHTGRAY),
    LIGHT_GREEN("Light Green", Color.LIGHTGREEN),
    LIGHT_BLUE("Light Blue", Color.LIGHTBLUE),
    //    ("", Color.),
    PURPLE("Purple", Color.PURPLE);

    private static final int DEFAULT_VALUES_COUNT = 1;
    private String colorName;
    private Color color;

    public static ColorEnum of(Paint color) {
        return Arrays.stream(values())
                .filter(colorEnum -> colorEnum.getColor().equals(color))
                .findAny()
                .orElseGet(() -> {
                    System.err.println("Cant find color: " + color);
                    return DEFAULT_COLOR;
                });
    }

    public static ColorEnum of(String colorName) {
        return Arrays.stream(values())
                .filter(colorEnum -> colorEnum.getColorName().equals(colorName))
                .findAny()
                .orElseGet(() -> {
                    System.err.println("Cant find color name: " + colorName);
                    return DEFAULT_COLOR;
                });
    }

    public static List<ColorEnum> getColorsWithoutDefaults() {
        return Arrays.stream(values())
                .filter(colorEnum -> !colorEnum.getColorName().equals(DEFAULT_COLOR.getColorName()))
                .collect(Collectors.toList());
    }

    public static ColorEnum getPreviousColor(ColorEnum colorEnum) {
        int index = Arrays.asList(values()).indexOf(colorEnum) - 1;

        return index >= DEFAULT_VALUES_COUNT ? values()[index] : values()[values().length - 1];
    }

    public static ColorEnum getNextColor(ColorEnum colorEnum) {
        int index = Arrays.asList(values()).indexOf(colorEnum) + 1;

        return index < values().length ? values()[index] : values()[DEFAULT_VALUES_COUNT];
    }

    public static List<ColorEnum> getListOfDefaultColors() {
        return Arrays.stream(values())
                .filter(colorEnum -> !getColorsWithoutDefaults().contains(colorEnum))
                .collect(Collectors.toList());
    }
}
