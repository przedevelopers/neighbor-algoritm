package algortim.enums;

public enum NeighborPosition {
    LEFT,
    RIGHT,
    UP,
    DOWN,
}
