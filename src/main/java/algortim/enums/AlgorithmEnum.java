package algortim.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AlgorithmEnum {
    FOUR_COLOR_MAX_OCCURS("Max of four colors"),
    RANDOM_COLOR("Change to random color"),
    TWO_COLOR_MAX_OCCURS_HORIZONTALLY("Max of two horizontally colors"),
    TWO_COLOR_MAX_OCCURS_VERTICALLY("Max of two vertically colors");

    private String algorithmName;
}
