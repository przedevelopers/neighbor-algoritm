package algortim.algorithms.implementations;

import algortim.algorithms.Algorithm;
import algortim.enums.NeighborPosition;
import algortim.model.PopulationElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.scene.paint.Paint;

public class TwoColorsMaxOccursHorizontally extends Algorithm {

    public TwoColorsMaxOccursHorizontally() {
        super();
    }

    @Override
    public List<PopulationElement> checkElement(List<PopulationElement> elementsRef, PopulationElement element, int index, int elementSize) {
        List<PopulationElement> neighbors = getNeighborsForElementWithIndex(elementsRef, elementSize, index, NeighborPosition.LEFT, NeighborPosition.RIGHT);
        Map<Paint, Integer> mapOfNeighborsColoursWithCount = getColoursFromPopulationValueDESC(neighbors);

        List<Paint> coloursOfNeighbors = new ArrayList<>(mapOfNeighborsColoursWithCount.keySet());

        Paint pickedColor = coloursOfNeighbors.size() > 0
                ? coloursOfNeighbors.get(rand.nextInt(coloursOfNeighbors.size()))
                : element.getFill();

        element.setFill(pickedColor);

        return elementsRef;
    }
}
