package algortim.algorithms.implementations;

import algortim.algorithms.Algorithm;
import algortim.model.PopulationElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.scene.paint.Paint;

public class RandomColorAlgorithm extends Algorithm {

    public RandomColorAlgorithm() {
    }

    @Override
    public List<PopulationElement> checkElement(List<PopulationElement> elementsRef, PopulationElement element, int index, int elementSize) {

        List<PopulationElement> neighbors = getNeighborsForElementWithIndex(elementsRef, elementSize, index);
        Map<Paint, Integer> colorsWithCount = getColoursFromPopulationValueDESC(neighbors);
        List<Paint> paints = new ArrayList<>(colorsWithCount.keySet());

        paints.add(element.getFill());

        element.setFill(paints.get(rand.nextInt(paints.size())));

        return elementsRef;
    }
}
