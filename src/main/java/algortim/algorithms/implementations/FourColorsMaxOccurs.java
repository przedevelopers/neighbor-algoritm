package algortim.algorithms.implementations;

import algortim.algorithms.Algorithm;
import algortim.model.PopulationElement;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javafx.scene.paint.Paint;

public class FourColorsMaxOccurs extends Algorithm {

    public FourColorsMaxOccurs() {
        super();
    }

    @Override
    public List<PopulationElement> checkElement(List<PopulationElement> elementsRef, PopulationElement element, int index, int elementSize) {
        List<PopulationElement> neighbors = getNeighborsForElementWithIndex(elementsRef, elementSize, index);
        Map<Paint, Integer> mapOfNeighborsColoursWithCount = getColoursFromPopulationValueDESC(neighbors);

        List<Paint> coloursOfNeighbors = mapOfNeighborsColoursWithCount.entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        Paint pickedColor = coloursOfNeighbors.size() > 0
                ? coloursOfNeighbors.get(rand.nextInt(coloursOfNeighbors.size()))
                : element.getFill();

        element.setFill(pickedColor);

        return elementsRef;
    }
}
