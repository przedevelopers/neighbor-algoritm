package algortim.algorithms;

import algortim.enums.ColorEnum;
import algortim.enums.NeighborPosition;
import algortim.model.PopulationElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import javafx.scene.paint.Paint;
import lombok.Setter;


import static java.util.stream.Collectors.toMap;

public abstract class Algorithm {
    protected Random rand;
    @Setter
    private int height;
    @Setter
    private int width;

    public Algorithm() {
        rand = new Random();
    }

    abstract public List<PopulationElement> checkElement(List<PopulationElement> elementsRef, PopulationElement element, int index, int elementSize);

    protected List<PopulationElement> getNeighborsForElementWithIndex(List<PopulationElement> elementsRef, int elementSize, int index, NeighborPosition... positions) {
        List<PopulationElement> neighbors = new ArrayList<>();

        List<NeighborPosition> positionsList = Arrays.asList(positions);

        if (positionsList.isEmpty() || positionsList.contains(NeighborPosition.LEFT)) {
            neighbors.add(getLeftNeighborOrNull(elementsRef, index, elementSize));
        }

        if (positionsList.isEmpty() || positionsList.contains(NeighborPosition.RIGHT)) {
            neighbors.add(getRightNeighborOrNull(elementsRef, index, elementSize));
        }

        if (positionsList.isEmpty() || positionsList.contains(NeighborPosition.UP)) {
            neighbors.add(getUpNeighborOrNull(elementsRef, index, elementSize));
        }

        if (positionsList.isEmpty() || positionsList.contains(NeighborPosition.DOWN)) {
            neighbors.add(getDownNeighborOrNull(elementsRef, index, elementSize));
        }

        return neighbors.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    protected PopulationElement getLeftNeighborOrNull(List<PopulationElement> elementsRef, int index, int elementSize) {
        if ((index % (width / elementSize)) - 1 >= 0) {
            return elementsRef.get(index - 1);
        }

        return null;
    }

    protected PopulationElement getRightNeighborOrNull(List<PopulationElement> elementsRef, int index, int elementSize) {
        if ((index % (width / elementSize)) + 1 < (width / elementSize)) {
            return elementsRef.get(index + 1);
        }

        return null;
    }

    protected PopulationElement getUpNeighborOrNull(List<PopulationElement> elementsRef, int index, int elementSize) {
        if (index - height / elementSize >= 0) {
            return elementsRef.get(index - height / elementSize);
        }

        return null;
    }

    protected PopulationElement getDownNeighborOrNull(List<PopulationElement> elementsRef, int index, int elementSize) {
        if ((index + width / elementSize) < elementsRef.size()) {
            return elementsRef.get(index + width / elementSize);
        }

        return null;
    }

    private Map<Paint, Integer> getColoursFromPopulationValueDESC(List<PopulationElement> population,
                                                                  Comparator<? super Map.Entry<Paint, Integer>> mapComparator) {
        Map<Paint, Integer> neighborsColorMap = new HashMap<>();

        population.forEach(nei -> {
            if (neighborsColorMap.containsKey(nei.getFill())) {
                neighborsColorMap.put(nei.getFill(), neighborsColorMap.get(nei.getFill()) + 1);
            } else {
                neighborsColorMap.put(nei.getFill(), 1);
            }
        });

        return neighborsColorMap
                .entrySet()
                .stream()
                .sorted(mapComparator)
                .collect(
                        toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new
                        )
                );
    }

    public Map<Paint, Integer> getColoursFromPopulationValueDESC(List<PopulationElement> population) {
        return getColoursFromPopulationValueDESC(population, Collections.reverseOrder(Map.Entry.comparingByValue()));
    }

    public Map<Paint, Integer> getColoursFromPopulationValueASC(List<PopulationElement> population) {
        return getColoursFromPopulationValueDESC(population, Map.Entry.comparingByValue());
    }

    public Map<Paint, Integer> getColoursFromPopulationKeyASC(List<PopulationElement> population) {
        return getColoursFromPopulationValueDESC(population, Comparator.comparing(a -> ColorEnum.of(a.getKey()).getColorName())
        );
    }

    public Map<Paint, Integer> getColoursFromPopulationKeyDESC(List<PopulationElement> population) {
        return getColoursFromPopulationValueDESC(population, (a, b) ->
                -1 * ColorEnum.of(a.getKey()).getColorName().compareTo(ColorEnum.of(b.getKey()).getColorName())
        );
    }
}
