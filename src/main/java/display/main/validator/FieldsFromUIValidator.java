package display.main.validator;

import algortim.enums.ColorEnum;
import javafx.scene.control.Alert;

public class FieldsFromUIValidator {
    private final int MIN_ELEMENT_SIZE = 10;
    private final int MIN_STEPS_TO_END = 2;
    private final int MIN_STEP_TIME = 10;
    private final int MIN_COLORS_COUNT = 1;

    private final int MAX_ELEMENT_SIZE = 400;
    private final int MAX_STEPS_TO_END = 1000;
    private final int MAX_STEP_TIME = 1000;
    private final int MAX_COLORS_COUNT = ColorEnum.getColorsWithoutDefaults().size();

    public boolean validateElementSize(int elementSize) {
        if (elementSize < MIN_ELEMENT_SIZE) {
            new Alert(Alert.AlertType.ERROR, createTextMin("Element size", elementSize, MIN_ELEMENT_SIZE)).show();
            return false;
        } else if (elementSize > MAX_ELEMENT_SIZE) {
            new Alert(Alert.AlertType.ERROR, createTextMax("Element size", elementSize, MAX_ELEMENT_SIZE)).show();
            return false;
        }

        return true;
    }

    public boolean validateStepTime(int stepTime) {
        if (stepTime < MIN_STEP_TIME) {
            new Alert(Alert.AlertType.ERROR, createTextMin("Step time", stepTime, MIN_STEP_TIME)).show();
            return false;
        } else if (stepTime > MAX_STEP_TIME) {
            new Alert(Alert.AlertType.ERROR, createTextMax("Step time", stepTime, MAX_STEP_TIME)).show();
            return false;
        }

        return true;
    }

    public boolean validateStepToEnd(int stepToEnd) {
        if (stepToEnd < MIN_STEPS_TO_END) {
            new Alert(Alert.AlertType.ERROR, createTextMin("Step To End", stepToEnd, MIN_STEPS_TO_END)).show();
            return false;
        } else if (stepToEnd > MAX_STEPS_TO_END) {
            new Alert(Alert.AlertType.ERROR, createTextMax("Step To End", stepToEnd, MAX_STEPS_TO_END)).show();
            return false;
        }

        return true;
    }

    public boolean validateColorsCount(int colorsCount) {
        if (colorsCount < MIN_STEPS_TO_END) {
            new Alert(Alert.AlertType.ERROR, createTextMin("Colors", colorsCount, MIN_COLORS_COUNT)).show();
            return false;
        } else if (colorsCount > MAX_STEPS_TO_END) {
            new Alert(Alert.AlertType.ERROR, createTextMax("Colors", colorsCount, MAX_COLORS_COUNT)).show();
            return false;
        }

        return true;
    }

    private String createTextMin(String variableName, int variableValue, int minValue) {
        return "Variable " + variableName.trim() + "(" + variableValue + ") is too small!\nMin value is " + minValue;
    }

    private String createTextMax(String variableName, int variableValue, int maxValue) {
        return "Variable " + variableName.trim() + "(" + variableValue + ") is too big!\nMax value is " + maxValue;
    }
}
