package display.main.controller;


import algortim.algorithms.Algorithm;
import algortim.enums.AlgorithmEnum;
import algortim.enums.ColorEnum;
import algortim.factory.AlgorithmFactory;
import algortim.model.PopulationElement;
import display.elements.CheckComboBox;
import display.main.validator.FieldsFromUIValidator;
import display.utils.UtilsField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/*
to fxml

<?import display.elements.CheckComboBox?>
<CheckComboBox fx:id="comboBoxColors" layoutX="55.0" layoutY="265.0" prefHeight="25.0" prefWidth="90.0"/>

 */
public class Controller implements Initializable {

    private final int DEFAULT_MARGIN = 1;
    private final int DEFAULT_RADIUS = 5;
    private final int DEFAULT_ELEMENT_SIZE = 10;
    private final int DEFAULT_STEPS_TO_END = 100;
    private final int DEFAULT_STEP_TIME = 100;
    private final int DEFAULT_SELECTED_COLOURS = 3;
    private final int DEFAULT_LAYOUT_X = 5;
    private final int DEFAULT_HEIGHT = 25;
    private final String BUTTON_TEXT_NEXT_STEP = "Next step";
    private final String BUTTON_TEXT_STOP = "Stop steps";
    private final String DEFAULT_COLOR_LABEL_ID_PREFIX = "colorLabel";

    @FXML
    Pane contentScene;
    @FXML
    Pane paneMenu;
    @FXML
    Label labelCurrentStep;
    @FXML
    Label labelElementsCount;
    @FXML
    Label labelColorsCount;
    @FXML
    Label labelAutoStep;
    @FXML
    TextField textFieldElementSize;
    @FXML
    TextField textFieldStepsToEnd;
    @FXML
    TextField textFieldStepTime;
    @FXML
    Button buttonAddPopulation;
    @FXML
    Button buttonClear;
    @FXML
    Button buttonNextStep;
    @FXML
    CheckBox checkBoxAutoSteps;
    @FXML
    ComboBox<String> comboBoxStrategy;
    @FXML
    CheckComboBox<String> comboBoxColors;

    private Control lowestElement;
    private Algorithm algorithm;
    private List<PopulationElement> elementsRef;
    private List<ColorEnum> colors;
    private int currentStep = 0;
    private int autoStep = 0;
    private int elementsShiftX = 0;
    private int elementsShiftY = 0;
    private int elementSize;
    private int stepsToEnd;
    private int stepTime;
    private int width;
    private int height;
    private boolean isAutomaticallyStepsOnGoing = false;
    private boolean stopAutomaticallySteps = true;
    private FieldsFromUIValidator validator;

    public Controller() {
        elementSize = DEFAULT_ELEMENT_SIZE;

        elementsRef = new ArrayList<>();
        colors = new ArrayList<>();

        validator = new FieldsFromUIValidator();
    }

    public void initialize(URL location, ResourceBundle resources) {
        textFieldElementSize.setText(DEFAULT_ELEMENT_SIZE + "");
        textFieldStepsToEnd.setText(DEFAULT_STEPS_TO_END + "");
        textFieldStepTime.setText(DEFAULT_STEP_TIME + "");

        labelCurrentStep.setText(currentStep + "");

        updateValuesFromUI();

        textFieldElementSize.setFocusTraversable(false);
        textFieldStepsToEnd.setFocusTraversable(false);
        textFieldStepTime.setFocusTraversable(false);

        buttonAddPopulation.setDisable(false);
        buttonClear.setDisable(true);
        buttonNextStep.setDisable(true);

        List<String> namesOfStrategies = Arrays.stream(AlgorithmEnum.values()).map(AlgorithmEnum::getAlgorithmName).collect(Collectors.toList());

        comboBoxStrategy.setItems(FXCollections.observableArrayList(namesOfStrategies));
        comboBoxStrategy.getSelectionModel().selectFirst();
        comboBoxStrategy.setDisable(false);

        comboBoxColors.setDisable(false);

        initComboBoxColors();

        buttonAddPopulation.setOnAction((event) -> addPopulation());
        buttonNextStep.setOnAction((event) -> makeSteps());
        buttonClear.setOnAction((event) -> resetAlgorithm());

        height = (int) contentScene.getPrefHeight();
        width = (int) contentScene.getPrefWidth();
    }

    private void findLowestElement() {
        paneMenu
                .getChildren()
                .forEach(
                        child -> {
                            if (lowestElement == null || lowestElement.getLayoutY() <= child.getLayoutY()) {
                                if (child instanceof Control) {
                                    lowestElement = (Control) child;
                                }
                            }
                        }
                );
    }

    private void initComboBoxColors() {
        List<String> colorNames = ColorEnum.getColorsWithoutDefaults()
                .stream()
                .map(ColorEnum::getColorName)
                .collect(Collectors.toList());

        comboBoxColors.getItems().addAll(colorNames);

        selectFirstElementsByDefault();
    }

    private void selectFirstElementsByDefault() {
        Stream.iterate(0, x -> x + 1)
                .limit(DEFAULT_SELECTED_COLOURS)
                .forEach(
                        index -> comboBoxColors.getItemBooleanProperty(index).setValue(true));
    }

    private void addPopulation() {
        findLowestElement();

        if (!validateValuesFromUI()) {
            return;
        }

        updateValuesFromUI();
        initPopulation();

        buttonAddPopulation.setDisable(true);
        buttonClear.setDisable(false);
        buttonNextStep.setDisable(false);

        comboBoxStrategy.setDisable(true);
        comboBoxColors.setDisable(true);

        textFieldElementSize.setDisable(true);

        updateStatistics();
    }

    private boolean validateValuesFromUI() {
        int colorsCount = (int) comboBoxColors.getCheckModel()
                .getCheckedItems()
                .stream()
                .map(ColorEnum::of).count();

        return validator.validateElementSize(UtilsField.getIntValueFromTextField(textFieldElementSize))
                && validator.validateStepTime(UtilsField.getIntValueFromTextField(textFieldStepsToEnd))
                && validator.validateStepToEnd(UtilsField.getIntValueFromTextField(textFieldStepTime))
                && validator.validateColorsCount(colorsCount);
    }


    private void updateValuesFromUI() {
        elementSize = UtilsField.getIntValueFromTextField(textFieldElementSize, DEFAULT_ELEMENT_SIZE);
        stepsToEnd = UtilsField.getIntValueFromTextField(textFieldStepsToEnd, DEFAULT_STEPS_TO_END);
        stepTime = UtilsField.getIntValueFromTextField(textFieldStepTime, DEFAULT_STEP_TIME);

        updateAlgorithmFromUIComboBox();
        updateColorsFromUIComboBox();
    }

    private void updateColorsFromUIComboBox() {
        colors = comboBoxColors.getCheckModel()
                .getCheckedItems()
                .stream()
                .map(ColorEnum::of)
                .collect(Collectors.toList());
    }

    private void updateAlgorithmFromUIComboBox() {
        int selectedValue = comboBoxStrategy.getSelectionModel().getSelectedIndex();
        selectedValue = selectedValue != -1 ? selectedValue : 0;
        algorithm = AlgorithmFactory.getAlgorithm(AlgorithmEnum.values()[selectedValue], width, height);
    }

    private void resetAlgorithm() {
        buttonAddPopulation.setDisable(false);
        buttonClear.setDisable(true);
        buttonNextStep.setDisable(true);

        comboBoxStrategy.setDisable(false);
        comboBoxColors.setDisable(false);

        textFieldElementSize.setDisable(false);

        isAutomaticallyStepsOnGoing = false;
        stopAutomaticallySteps = true;

        contentScene.getChildren().clear();
        elementsRef.clear();

        currentStep = 0;
        autoStep = 0;

        deleteAllColorLabels();
        updateStatistics();
    }

    private void updateStatistics() {
        labelCurrentStep.setText(currentStep + "");
        labelAutoStep.setText(autoStep + "");
        labelElementsCount.setText(elementsRef.size() + "");
        labelColorsCount.setText(countColorsOfElements() + "");
        lowestElement = null;
        deleteAllColorLabels();
        generateMessageOfColorsWithCountsLit();
    }

    private void generateMessageOfColorsWithCountsLit() {
        Map<Paint, Integer> colorsFromPopulation = algorithm.getColoursFromPopulationValueDESC(elementsRef);

        colorsFromPopulation
                .forEach((key, value) -> {
                    ColorEnum color = ColorEnum.of(key);
                    boolean contain = paneMenu
                            .getChildren()
                            .stream()
                            .map(Node::getId)
                            .filter(Objects::nonNull)
                            .anyMatch(id -> id.startsWith(DEFAULT_COLOR_LABEL_ID_PREFIX + color.getColorName()));

                    if (contain) {
                        changeExistingLabel(color, value);
                    } else {
                        findLowestElement();
                        createAndAddLabel(color, value);
                    }
                });
    }

    private void changeExistingLabel(ColorEnum color, int value) {
        paneMenu
                .getChildren()
                .stream()
                .filter(child -> child instanceof Label)
                .filter(child -> child.getId() != null)
                .filter(child -> child.getId().equals(DEFAULT_COLOR_LABEL_ID_PREFIX + color.getColorName()))
                .findFirst()
                .map(child -> (Label) child)
                .ifPresent(label -> {
                            label.setText(color.getColorName() + ": " + value);
                            label.setTextFill(Color.BLACK);
                        }
                );
    }

    private void deleteAllColorLabels() {
        lowestElement = null;
        List<Node> elementsToDelete =
                paneMenu
                        .getChildren()
                        .stream()
                        .filter(child -> child instanceof Label)
                        .filter(child -> child.getId() != null)
                        .filter(child -> child.getId().startsWith(DEFAULT_COLOR_LABEL_ID_PREFIX))
                        .collect(Collectors.toList());

        paneMenu.getChildren().removeAll(elementsToDelete);
    }

    private Label createAndAddLabel(ColorEnum color, int value) {
        Label label = new Label(color.getColorName() + ": " + value);
        label.setId(DEFAULT_COLOR_LABEL_ID_PREFIX + color.getColorName());
        label.setTextFill(Color.BLACK);
        label.setLayoutY(lowestElement.getLayoutY() + lowestElement.getPrefHeight());
        label.setLayoutX(DEFAULT_LAYOUT_X - DEFAULT_MARGIN);
        label.setPrefHeight(DEFAULT_HEIGHT);
        label.setPrefWidth(paneMenu.getPrefWidth() - 2 * (DEFAULT_LAYOUT_X - DEFAULT_MARGIN));
        label.setBackground(
                new Background(
                        new BackgroundFill(
                                color.getColor(),
                                new CornerRadii(DEFAULT_RADIUS),
                                new Insets(DEFAULT_MARGIN)
                        )
                )
        );
        label.setAlignment(Pos.CENTER);

        paneMenu.getChildren().add(label);
        return label;
    }

    private long countColorsOfElements() {
        return elementsRef.stream()
                .map(PopulationElement::getFill)
                .distinct()
                .count();
    }

    private void doStepsAutomatically() {
        isAutomaticallyStepsOnGoing = true;
        stopAutomaticallySteps = false;

        new Thread(this::automaticallyStepsTask).start();
    }

    private void automaticallyStepsTask() {
        for (int f = 0; !shouldStopOnIndex(f); f++) {
            Platform.runLater(this::doNextStepToAllElements);
            autoStep = f + 1;
            tryToSleep();
        }

        isAutomaticallyStepsOnGoing = false;
        stopAutomaticallySteps = true;
        Platform.runLater(() -> {
                    updateStatistics();
                    buttonNextStep.setText(BUTTON_TEXT_NEXT_STEP);
                }
        );
    }

    private boolean shouldStopOnIndex(int f) {
        return (stepsToEnd != -1 && f >= stepsToEnd)
                || stopAutomaticallySteps
                || countColorsOfElements() <= 1;
    }

    private void tryToSleep() {
        try {
            Thread.sleep(stepTime);
        } catch (Exception e) {
            System.err.println("problem with Thread.sleep()");
        }
    }

    private void initPopulation() {
        int height = (int) contentScene.getPrefHeight();
        int width = (int) contentScene.getPrefWidth();

        elementsShiftX = (int) ((height % elementSize) / 2.0);
        elementsShiftY = (int) ((width % elementSize) / 2.0);

        for (int f = 0; f < height / elementSize; f++) {
            for (int g = 0; g < width / elementSize; g++) {
                contentScene.getChildren().add(PopulationElement.of(colors, elementSize, elementSize,
                        elementsShiftX + g * elementSize, elementsShiftY + f * elementSize));
            }
        }

        elementsRef = contentScene.getChildren()
                .filtered(kid -> kid instanceof PopulationElement)
                .stream()
                .filter(kid -> kid instanceof PopulationElement)
                .map(kid -> ((PopulationElement) kid))
                .collect(Collectors.toList());
    }

    private void makeSteps() {
        updateValuesFromUI();
        if (isAutomaticallyStepsOnGoing) {
            stopAutomaticallySteps = true;
            isAutomaticallyStepsOnGoing = false;
            return;
        }

        if (checkBoxAutoSteps.isSelected()) {
            buttonNextStep.setText(BUTTON_TEXT_STOP);
            doStepsAutomatically();
            return;
        }

        doNextStepToAllElements();
    }

    private void doNextStepToAllElements() {
        IntStream.range(0, elementsRef.size())
                .forEach(index -> checkNeighborsOfElementAlgorithm(elementsRef.get(index), index));

        currentStep++;
        updateStatistics();
    }

    private void checkNeighborsOfElementAlgorithm(PopulationElement element, int index) {
        algorithm.checkElement(elementsRef, element, index, elementSize);
    }
}

