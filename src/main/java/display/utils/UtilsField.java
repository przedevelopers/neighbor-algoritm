package display.utils;

import javafx.scene.control.TextField;

public class UtilsField {
    private final static int DEFAULT_INT_VALUE = -1;

    public static int getIntValueFromTextField(TextField textField, int defaultValue) {
        try {
            return Integer.parseInt(textField.getText());
        } catch (NumberFormatException nfe) {
            System.err.println("[ERROR] -| " + textField.getId() + " |- Cannot parse value: \"" + textField.getText() + "\" ~ Returned default value: \"" + defaultValue + "\"");
        }

        return defaultValue;
    }

    public static int getIntValueFromTextField(TextField textField) {
        return getIntValueFromTextField(textField, DEFAULT_INT_VALUE);
    }
}
